from pymongo import MongoClient
import mysql.connector

mydb = mysql.connector.connect(
  host = "10.100.100.2",
  user = "admin",
  passwd = "test",
  database = "database"
)

client = MongoClient("10.100.100.1")
baza = client["lab1_db"]
kolekcja = baza["lab1"]

kolekcja.drop()
print("Kolekcja MongoDB dropped!")

mycursor = mydb.cursor()
mycursor.execute("DROP TABLE lab1")
print("Tabela MariaDB dropped!")