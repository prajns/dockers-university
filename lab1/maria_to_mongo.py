from pymongo import MongoClient
import mysql.connector

mydb = mysql.connector.connect(
  host = "10.100.100.2",
  user = "admin",
  passwd = "test",
  database = "database"
)

client = MongoClient("10.100.100.1")
baza = client["lab1_db"]
kolekcja = baza["lab1"]

collections = baza.list_collection_names()
if "lab1" in collections:
    print('Collection exists!')
else:
    kolekcja = baza["lab1"]

mycursor = mydb.cursor()
mycursor.execute("SELECT * FROM lab1")
myresults = mycursor.fetchall()

columns_names = [i[0] for i in mycursor.description]

data_arr = []

for record in myresults:
    data_arr.append({columns_names[0]:record[0], columns_names[1]:record[1]})

kolekcja.insert_many(data_arr)