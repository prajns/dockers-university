import mysql.connector
import string
import random
def id_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

mydb = mysql.connector.connect(
  host="10.100.100.2",
  user="admin",
  passwd="test",
  database="database"
)

mycursor = mydb.cursor()

sql = "CREATE TABLE IF NOT EXISTS `lab1` (`id` INT AUTO_INCREMENT PRIMARY KEY, `wartosc` varchar(20))"
mycursor.execute(sql)

N = 1000
data_arr = []
for i in range(N):
    war = (id_generator(),)
    data_arr.append(war)

sql = "INSERT INTO lab1 (wartosc) VALUES (%s)"

mycursor.executemany(sql, data_arr)
mydb.commit()

print(mycursor.rowcount, "losowych rekordow zostalo wstawionych")