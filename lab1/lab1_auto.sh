#!/bin/bash
source bin/activate
python -m pip install mysql-connector
python -m pip install pymongo
echo 'Skrypt do tworzenia i wypelnienia bazy Maria:'
time python sql_create_and_fill_table.py
echo 'Skrypt do pobierania rekordow z MariaDB i wstawienia do MongoDB:'
time python maria_to_mongo.py
echo 'Skrypt do wyszyczczenia danych Mongo i Maria:'
python clear_dbs.py