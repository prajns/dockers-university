from pymongo import MongoClient
import string
import random
def id_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

klient = MongoClient("10.100.100.1")
baza = klient["lab2_db1"]
kolekcja = baza["lab2"]

N = 10
data_arr = []

for i in range(N):
    data_arr.append({"id":i, "wartosc":id_generator()})

x = kolekcja.insert_many(data_arr)
print(N, " losowych rekordow zostalo wstawionych")