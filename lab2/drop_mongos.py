from pymongo import MongoClient

klient1 = MongoClient("10.100.100.1")
baza1 = klient1["lab2_db1"]
kolekcja1 = baza1["lab2"]

klient2 = MongoClient("10.100.100.2")
baza2 = klient2["lab2_db2"]
kolekcja2 = baza2["lab2"]

kolekcja1.drop()
print("Kolekcja MongoDB_1 dropped!")

kolekcja2.drop()
print("Kolekcja MongoDB_2 dropped!")